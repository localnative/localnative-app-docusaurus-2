module.exports = {
  someSidebar: {
    "Quick Start": ["quick-start","videos"],
    "Developer": ["developer-setup"
      , "source-code"
      , "release"
      , "changelog"
      , "todo"
      , "translation"
      , "license"
    ],
    "Lecture & Talk": ["lecture", "talk"],
    "Summer of Code": [
        "localnative-2021-soc"
    ],
    "Rust GUI iced 中文教程": [
      "gui-iced/tutorial0",
      "gui-iced/tutorial1",
      "gui-iced/tutorial2",
      "gui-iced/tutorial3",
      "gui-iced/tutorial4",
      "gui-iced/tutorial5",
      "gui-iced/tutorial6",
      "gui-iced/tutorial7",
      "gui-iced/tutorial8",
      "gui-iced/tutorial9",                  
    ]
  },
};
