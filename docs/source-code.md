---
id: source-code 
title: Source Code
---

[https://gitlab.com/localnative/localnative](https://gitlab.com/localnative/localnative)

[git-ssb](https://git.scuttlebot.io/%25q13hLJchNXz%2FxZi9mjWVHyIbRnkr5VjF0Y6BfhrOV6Q%3D.sha256)
`ssb://%q13hLJchNXz/xZi9mjWVHyIbRnkr5VjF0Y6BfhrOV6Q=.sha256`

[https://git.sr.ht/~yiwang/localnative](https://git.sr.ht/~yiwang/localnative)

## Contribution
[Contributor Covenant Code of Conduct](https://www.contributor-covenant.org/version/1/4/code-of-conduct)

## Developer Certificate of Origin
Inspired by and borrowed from [Librem5](https://developer.puri.sm/Librem5/Appendix/dco.html).

The Developer Certificate of Origin (DCO) ensures that persons contributing code are allowed to do so and that the project stays under a free license. This is indicated by a sign-off.

The sign-off is a simple line at the end of the explanation for the patch, which certifies that you wrote it or otherwise have the right to pass it on as an open-source patch. The rules are pretty simple: if you can certify the below:

```
Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I
    have the right to submit it under the open source license
    indicated in the file; or

(b) The contribution is based upon previous work that, to the best
    of my knowledge, is covered under an appropriate open source
    license and I have the right under that license to submit that
    work with modifications, whether created in whole or in part
    by me, under the same open source license (unless I am
    permitted to submit under a different license), as indicated
    in the file; or

(c) The contribution was provided directly to me by some other
    person who certified (a), (b) or (c) and I have not modified
    it.

(d) I understand and agree that this project and the contribution
    are public and that a record of the contribution (including all
    personal information I submit with it, including my sign-off) is
    maintained indefinitely and may be redistributed consistent with
    this project or the open source license(s) involved.
```

then you just add a line saying:

```
Signed-off-by: Developer Name <name@developer.example.org>
```

using your real name (sorry, no pseudonyms or anonymous contributions.)
