---
id: release
title: Release
---

## Installer packages
[https://gitlab.com/localnative/localnative-release](https://gitlab.com/localnative/localnative-release)

## Libraries (deprecated)

Those library artifacts are not updated for newer releases, please build from the newest source code.

### rust crates
| description  | url |
|--------------|-----|
| web extension host | [https://crates.io/crates/localnative_cli](https://crates.io/crates/localnative_cli) |
| core lib | [https://crates.io/crates/localnative_core](https://crates.io/crates/localnative_core) |
| ssb lib (deprecated) | [https://crates.io/crates/localnative_ssb](https://crates.io/crates/localnative_ssb) |

### nodejs npm
| description  | url |
|--------------|-----|
| nodejs binary (deprecated) | [https://www.npmjs.com/package/localnative](https://www.npmjs.com/package/localnative) |
| nodejs to rust bridge | [https://www.npmjs.com/package/localnative-neon](https://www.npmjs.com/package/localnative-neon) |
