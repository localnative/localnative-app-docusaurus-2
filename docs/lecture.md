---
id: lecture
title: Lecture
---

- Develop Decentralized Cross-platform Apps - Software Engineering Exercise
    - [https://hx2020.localnative.app](https://hx2020.localnative.app)
    - [https://hx2019.localnative.app](https://hx2019.localnative.app)
