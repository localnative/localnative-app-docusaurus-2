---
id: talk
title: Talk
---

- [2021-07-04](https://localnative.app/blog/2021/07/04/localnative-dweb-community-call), DWeb Community Call, Offline First 的书签应用 Local Native
  
- [2021-03-30](https://localnative.app/blog/2021/03/30/localnative-rust-talk), 《Rust唠嗑室》第21期, 详解 Rust 实现的跨平台去中心的应用 Local Native
  
- [2019-09-26](https://localnative.app/blog/2019/09/30/localnative-rust-nyc-meetup), [Rust NYC](https://github.com/rust-nyc/meetups/issues/13), [2019 September Meetup](https://www.meetup.com/Rust-NYC/events/264849068/)
  - Local Native: A Decentralized Cross-platform App Developed with Rust
    - [slides](https://rust-nyc-2019.localnative.app/)
    - [screen recording (remake)](https://youtu.be/Qvhw8ace6KQ)
