---
title: "Local Native 2021 Summer of Code - Rust GUI"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all", "gui", "rust", "druid", "iced", "localnative", "contributor", "2021", "SoC"]
---
[2021-03-20 更新](2021-04-01-localnative-2021-soc-team.md)

### 目标
推进 FLOSS 项目 https://localnative.app 的 Rust GUI 代码和相关的 educative 教程的编写, 招募代码, 文档和教程贡献同学。
- Rust GUI 编程 
  - 用druid 和/或者 [iced](https://github.com/hecrj/iced) 框架 设计, 实现并取代目前用 Electron 实现的 Local Native 桌面程序
      - 尝试用 [druid](https://github.com/linebender/druid) 实现QR Code
  - 尝试支持 Windows 发布版本 (尝试用Rust修改[注册表](https://developer.chrome.com/docs/apps/nativeMessaging/#native-messaging-host-location))
  
- 给 educative.io 上相关课程加入新实现的内容
  
- 写计划Blog 和 总结 Blog, 完善文档 

### 时间线
借鉴G家2021 SoC的[时间线](https://summerofcode.withgoogle.com/how-it-works/#timeline)

日期| 事件 | 津贴
------|-----|-----
March 29 - April 13|申请 |
April 13 - May 17|确定申请结果
May 17 - June 7 | 熟悉代码和框架，写计划 Blog | 
June 7 - July 18 | code() and debug() and document() | July 18 First Evaluation 45%
July 18 - August 16 | code() and debug() and document() | 
August 16-31 | 写总结 Blog 和 educative 教程文档 | August 31 Final Evaluation 55%


### 津贴
- 参考G家SoC [Stipends](https://developers.google.com/open-source/gsoc/help/student-stipends)
- 支付用 PayPal 通过 [Open Collective](https://opencollective.com/localnative)


欢迎有Rust经验的同学申请!

备注 SoC 发送 
- 简历
- Rust经验/作品 
- 期望津贴 
