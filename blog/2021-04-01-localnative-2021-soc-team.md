---
title: "Local Native 2021 Summer of Code Team"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["2021 Summer of Code"]
tags: ["all", "localnative", "team", "2021", "SoC"]
---

提前完成了 [Local Native 2021 Summer of Code](2021-03-07-localnative-2021-soc-rust-gui.md) 团队的组建。

## 时间线
调整和提前了主要时间线:

日期| 事件 | Note
------|-----|-----
April 3 - 24 | 熟悉代码和框架，写计划 Blog |
April 24 - June 5 | code() and debug() and document() | June 5 Evaluation
June 5 - July 3 | code() and debug() and document() |
July 3-17 | 写总结 Blog 和 educative 教程文档 | July 17 Evaluation
TBD | educative 审核上线 |




## 成员和主要目标
- Cupnfish
    - 实现并取代目前用 Electron 实现的 Local Native 桌面程序
    - 实现可使用的 Windows 发布版本
    - 在 localnative.app 网站 Blog 上写要在 educative.io 上加入 2021 SoC 新实现的内容的中文版
    - 写2021 SoC计划 Blog 和总结 Blog

- hill
    - 协助实现代码和技术文档等工作

- Conan
    - 项目管理达成目标：组织会议，在 localnative.app 写项目周报，完善文档。
    - 在 educative.io 上加入 2021 SoC 新实现的内容(翻译和完善)
    - 建立并完善 gitlab CI/CD 流程
    - 完善Browser Extension UI
    - 编写 Rust 代码

