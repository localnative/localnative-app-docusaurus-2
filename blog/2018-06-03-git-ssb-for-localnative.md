---
title: 用 git ssb 创建 localnative
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["评论 Commentary"]
tags: ["all", "localnative","git","ssb","trademark", "property", "gpl"]
---

本来想在 github 上建立 [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html) 的 localnative，回忆起 `myrepos` 的作者在某个时刻[删除](https://joeyh.name/blog/entry/removing_everything_from_github/)了所有 github 上他的代码，源于服务条款的升级对 copylefted 自由软件埋下的[潜在问题](https://www.mirbsd.org/permalinks/wlog-10_e20170301-tg.htm#e20170301-tg_wlog-10)。

而 [git ssb](https://git.scuttlebot.io/%25n92DiQh7ietE%2BR%2BX%2FI403LQoyf2DtR3WQfCkDKlheQU%3D.sha256) 的能力似乎足够。


归根到底是[产权](https://en.wikipedia.org/wiki/License_compatibility)问题，
<!--truncate-->

联想起商标和[软件许可](https://choosealicense.com/licenses/)是[不同](https://wptavern.com/the-gpl-license-doesnt-provide-the-freedom-to-infringe-registered-trademarks)的事情。

Linux 商标的[往事](https://youtu.be/UjDQtNYxtbU).

Caddy的[改变](https://www.digitalocean.com/community/tutorials/how-to-host-a-website-with-caddy-on-ubuntu-16-04)，电影 Mad Max 里的 property, 和西部世界里讲的IP。
