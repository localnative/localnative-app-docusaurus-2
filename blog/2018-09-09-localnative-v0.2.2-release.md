---
title: "Local Native v0.2.2 发布"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all", "localnative", "i18n",  "gitlab"]
---

![localnative-web-extension-v0.2.2.png](/img/localnative-web-extension-v0.2.2.png)

[这个](https://addons.mozilla.org/en-US/firefox/addon/localnative/)[版本](https://chrome.google.com/webstore/detail/local-native/oclkmkeameccmgnajgogjlhdjeaconnb)加入了中文i18n。

<!--truncate-->

[代码](https://gitlab.com/yiwang/localnative)放到了gitlab上。
