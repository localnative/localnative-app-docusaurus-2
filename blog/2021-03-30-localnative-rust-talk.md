---
title: "详解 Rust 实现的跨平台去中心的应用 Local Native"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all", "localnative", "talk", "2021", "rust"]
---

**2021-03-30《Rust唠嗑室》第21期**

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hNOy_Z8qonA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

