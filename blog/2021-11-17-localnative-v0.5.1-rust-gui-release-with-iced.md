---
title: "Local Native v0.5.1 Rust GUI Release with iced"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all", "localnative", "release", "iced", "windows"]
---


![release](/img/unsplash/tim-mossholder-m4qXRqi-EZI-unsplash.jpg)


This is the first version of [Local Native](https://localnative.app) Rust GUI desktop application ([iced](https://github.com/iced-rs/iced) implementation).

- We finally support Windows!
- macOS release is still in dmg format.
- Appimage build has some [issue](https://gitlab.com/localnative/localnative/-/issues/53).