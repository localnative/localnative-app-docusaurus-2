---
title: Local Native @ Rust NYC 2019 September Meetup
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all", "localnative", "rust", "nyc", "meetup", "talk"]
---

I've given a talk at [Rust NYC](https://github.com/rust-nyc/meetups/issues/13) for its [2019 September Meetup](https://www.meetup.com/Rust-NYC/events/264849068/).

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Qvhw8ace6KQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Above is the screen capture video remake when going over the [slides](https://rust-nyc-2019.localnative.app/) again with my HXNYC 2019 [class](https://hx2019.localnative.app).
