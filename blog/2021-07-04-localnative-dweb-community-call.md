---
title: "Offline First 的书签应用 Local Native"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all", "localnative", "talk", "2021", "dweb"]
---

**2021-07-04 DWeb Community Call**

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Znk-r5OFcx8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

- hand drawing diagram of uuid sync process used by Local Native
    - no update
    - no deletion

![uuid sync](/img/talk/localnative-uuid-sync.jpg)
