---
title: "Local Native v0.5.0 Electron and Mobile Release"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all", "localnative", "release"]
---

![release](/img/unsplash/ankush-minda-TLBplYQvqn0-unsplash.jpg)

[Local Native](https://localnative.app) v0.5.0 Electron release (appimage, dmg) should mark as the last version of Electron code.

Local Native iOS & iPadOS app and Android app also updated their `localnative_core` code, which
  - fixed syncing issue 
  - removed unused `ssb` table in database, 
  - and made upgrades to underlying Rust libraries.
