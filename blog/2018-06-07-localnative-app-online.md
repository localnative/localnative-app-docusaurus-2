---
title: "https://localnative.app 上线"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all","localnative","https"]
---

Mozilla 上有人给了一星，好像和 [Waterfox](https://www.waterfoxproject.org) 有关，“被迫”要上线网站提供上下文。
<!--truncate-->

用 s3 redirect 不行，怀疑是 `.app` 哪里强制用 https 的问题。
