---
title: "Local Native v0.3.10 发布"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all", "localnative", "release"]
---
This release adds QR code display for url.

- Mobile:

<img src="/img/localnative-android-0.3.10-p2-qrcode.png" width="200" />
<img src="/img/localnative-android-0.3.10-n5x.png" width="200" />

<!--truncate-->

- Desktop:
![localnative-desktop-0.3.10-qrcode-zh.jpg](/img/localnative-desktop-0.3.10-qrcode-zh.jpg)
