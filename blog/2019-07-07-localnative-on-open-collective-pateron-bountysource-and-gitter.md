---
title: Local Native is on Open Collective, Pateron, Bountysource and Gitter
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all", "localnative", "patreon", "opencollective", "bountysource", "gitter"]
---

It's straight forward to setup [Open Collective](https://opencollective.com/localnative) (takes 10% with fiscal host) and
[Pateron](https://www.patreon.com/localnative) (takes 8% with tier and goal features).
<!--truncate-->
The tier setup and wording may still need some tweaking, much inspiration from [redox](https://www.patreon.com/redox_os) and [manyverse](https://opencollective.com/manyverse).

Also [Bountysource](https://www.bountysource.com/teams/localnative-bounty/issues) and  [Gitter](https://gitter.im/localnative-app).

[Prior](https://github.com/nayafia/lemonade-stand) [art](https://hackernoon.com/why-funding-open-source-is-hard-652b7055569d) for reference.
