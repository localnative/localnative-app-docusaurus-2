---
title: "Local Native iOS Has 100 Downloads from Oct 2019 to April 2020"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all", "localnative", "download", "ios"]
---
100 seems the highest 26 weeks number I see so far.
<img src="/img/localnative-ios-downloads-26w-2020-04.png" width="300"></img>
