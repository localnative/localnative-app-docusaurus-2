---
title: "Local Native v0.5.1 Browser Extension Release"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["release", "2022", "localnative", "extension", "bountysource"]
---

![release](/img/localnative-0.5.1-browser-ext.png)

[Local Native](https://localnative.app/) v0.5.1 [Firefox Addon](https://addons.mozilla.org/en-US/firefox/addon/localnative/) is released with tweaks to the pagination information displayed being consistent with desktop application that uses format of
 ```
 ${start}-${end} / ${count}
 ```
 like
 ```
 1-10 / 13258
 ```
with [this code](https://gitlab.com/localnative/localnative/-/merge_requests/45/diffs#8c74cec3e97f9152af8ad2a2a8cf1f6120952555_201_203) and the [bounty](https://app.bountysource.com/issues/76235840-21-fix-web-extension-pagination-behaviour) is finally closed and paid out.

v0.5.1 [Chrome Extension](https://chrome.google.com/webstore/detail/local-native/oclkmkeameccmgnajgogjlhdjeaconnb) is pending on review and should be out soon.