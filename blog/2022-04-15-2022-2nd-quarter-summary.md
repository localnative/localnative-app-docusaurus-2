---
title: "2022 2nd Quarter Summary"
author: Cupnfish
authorURL: https://github.com/Cupnfish
authorImageURL: https://avatars.githubusercontent.com/u/40173605?v=4

categories: ["公告 Announcement"]
tags: ["summary", "2022", "localnative"]
---

Almost halfway through the second quarter, there have been no major updates or changes.

## Internal Enhancement

- [#](https://gitlab.com/localnative/localnative/-/issues/63) iOS add build script