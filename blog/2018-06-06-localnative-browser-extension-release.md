---
title: "Local Native 浏览器插件发布"
author: Yi Wang
authorURL: https://www.yi-wang.me
authorImageURL: https://secure.gravatar.com/avatar/1484b2bde1c0027dab9b135a1e051b3b?s=180&d=identicon

categories: ["公告 Announcement"]
tags: ["all","localnative","extension","github"]
---

谷歌5美元验证之后才能发布在Chrome Web Store。

Mozilla发布之前的自动检测不错。

用原生应用通信需要好几步才能配置好插件，技术门槛太高。
<!--truncate-->

如果做一个安装器支持主流的浏览器和操作系统都是很多工作量。

难怪Electron[无敌](https://medium.com/@felixrieseberg/defeating-electron-e1464d075528)。

要么自己做，要么受制于人。

预测 github 一两年之后会有[变化](https://www.youtube.com/watch?v=7c4ZMlXRQF0)，ssb上协作工具的呼声。
